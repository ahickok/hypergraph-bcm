# -*- coding: utf-8 -*-
"""
Numerical simulations used in our hypergraph BCM paper.
"""
from hbc import Hbc
from hbc import Distribution
import numpy as np
import random
import math
from scipy import stats
from scipy.special import comb
from scipy.stats import norm
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import seaborn as sns

def complete_sim(dist, N, num_trials = 1000, print_progress = True):
    # Run simulations on complete hypergraph and record the number that reach consensus.
    # dist is a Distribution object
    num_converged = 0
    for i in range(num_trials):
        opinions = dist.generate(N)
        H = Hbc(N, opinions, on_the_fly = True)
        T, _ = H.simulate_until_no_record(Hbc.consensus, MaxT = int(1e6), print_progress = print_progress)
        if T < int(1e6): 
            num_converged += 1
        if print_progress: print(f"Simulations that converged: {num_converged} out of {i+1}")
    return num_converged
    
def complete_sim_plot(dist, N, first_conc = True, print_progress = True):
    # first_conc = True means that we require the first hyperedge we choose to be concordant.
    opinions = dist.generate(N)
    H = Hbc(N, opinions, on_the_fly = True)
    H.simulate_until(Hbc.consensus, first_conc = first_conc, print_progress = print_progress)
    H.plot_record(first_conc = first_conc, title = "Complete Hypergraph")
 
def hsbm_sim(dist1, dist2, N):
    # Runs simulation on (p, q, M)-HSBM hypergraph with p = 1, q = 1, M = 2 
    # and plots results.
    # N must be even. Initial opinions for community 1 are drawn from dist1,
    # initial opinions for community 2 are drawn from dist2.
    opinions = [dist1.generate() if i < int(N/2) else dist2.generate() for i in range(N)]
    H = Hbc(N, opinions, on_the_fly = True, p = 1, q = 1, hsbm_bounded = True)
    H.simulate_until(Hbc.polarized)
    H.plot_record()
    
def enron_sim(dist, num_trials = 1000, print_progress = True):
    # Run simulations on Enron hypergraph and record number that reach consensus
    enron = Hbc.benson_graph('enron/email-Enron-nverts.txt', 'enron/email-Enron-simplices.txt', dist)
    num_converged = 0
    for i in range(num_trials):
        T, _ = enron.simulate_until_no_record(Hbc.converged, MaxT = int(1e6), print_progress = print_progress)
        if T < int(1e6):
            num_converged += 1
        if print_progress: print(f"Simulations that converged: {num_converged} out of {i+1}")
        opinions = dist.generate(enron.N)
        enron.reset(opinions)
    return num_converged

def enron_sim_plot(dist, print_progress = True):
    enron = Hbc.benson_graph('enron/email-Enron-nverts.txt', 'enron/email-Enron-simplices.txt', dist)
    enron.simulate_until(Hbc.converged, MaxT = int(1e6), print_progress = print_progress)
    enron.plot_record(title = "Enron Email Hypergraph")
    
def Gnm_sim(dist, N = 1000, m = 100, num_trials = 1000, print_progress = True):
    num_converged = 0
    for i in range(num_trials):
        if print_progress: print(f"Trial {i}")
        opinions = dist.generate(N)
        H = Hbc.sparse(N, m, opinions)
        if print_progress: print("Made the hypergraph")
        T, _ = H.simulate_until_no_record(Hbc.converged, MaxT = int(1e6), print_progress = print_progress)
        if T < int(1e6):
            num_converged += 1
        if print_progress: print(f"Simulations that converged: {num_converged} out of {i+1} trials")
    return num_converged

def Gnm_sim_plot(dist, N = 1000, m = 100, print_progress = True):
    opinions = dist.generate(N)
    H = Hbc.sparse(N, m, opinions)
    if print_progress: print("Made the hypergraph")
    H.simulate_until(Hbc.converged, print_progress = print_progress)
    H.plot_record()
    
def jump_sim(N = 1000, num_m_trials = 200, num_trials_per_m = 500):
    sig_vals = [.6, .8, 1, 1.2]
    colors = ['c', 'tab:orange', 'darkgreen', 'm']  # good colorblind palette
    c = Hbc.bound
    plt.figure(figsize = (10, 10))
    plt.rcParams.update({'font.size': 30})
    for i in range(len(sig_vals)):
        sigma = sig_vals[i]
        color = colors[i]
        print(f"sigma = {sigma}")
        x_vals = np.zeros(num_m_trials)
        mean_Js = np.zeros(num_m_trials)
        mean_sizes = np.zeros(num_m_trials)
        for k in range(num_m_trials):
            x = random.random()
            x_vals[k] = x
            m = [comb(N, i)*math.pow(x,i) for i in range(2,N+1)]
            M = sum(m)
            prob = [m[i]/M for i in range(N-1)]
            mean_size = sum([m[i]*(i+2)/M for i in range(N-1)])
            J = np.zeros(num_trials_per_m)
            for j in range(num_trials_per_m):
                opinions = np.random.normal(0, sigma, N)
                i = np.random.choice(range(2, N+1), p = prob)
                e = random.sample(range(N), i)
                ops_e = [opinions[n] for n in e]
                mean_e = sum(ops_e)/i
                discordance = 0
                for n in e:
                    discordance += (opinions[n] - mean_e)**2
                discordance /= (i-1)
                if discordance < c:
                    for n in e:
                        if abs(opinions[n] - mean_e) > c:
                            J[j] += 1
            mean_J = sum(J)/num_trials_per_m
            mean_Js[k] = mean_J
            mean_sizes[k] = mean_size
        sns.regplot(x = mean_sizes, y = mean_Js, ci = None, color = color, scatter_kws={'s':15})
        print(stats.pearsonr(mean_sizes, mean_Js))
    plt.suptitle(f"num hypergraphs = {num_m_trials}, num trials = {num_trials_per_m}")
    plt.xlabel(r"$\mathrm{\mathbb{E}}[|e_0|]$")
    plt.ylabel(r"$J_0$")
    patches = [mpatches.Patch(color=colors[i], label=fr'$\sigma$ = {sig_vals[i]}') for i in range(len(sig_vals))]
    plt.legend(handles=patches)

def plot_avg_conc_edge_size(N, sigma, num_trials):
    plt.rcParams.update({'font.size': 18})
    avg, _, _ = Hbc.approx_complete_conc_edge_size(N, sigma, num_trials)
    plt.plot(range(2, N), avg[2: N], 'o')
    plt.xlabel(r"$N$")
    plt.ylabel(r"$|\hat{e}|$")
    plt.show()
    
def plot_prob_conc(N, num_trials, sigma):
    # N is max edge size to consider
    # frac_conc[n] is an approximation to the probability that a size n hyperedge is concordant
    _, frac_conc, _ = Hbc.approx_complete_conc_edge_size(N+1, sigma, num_trials)
    plt.plot(range(2, N+1), frac_conc[2:N+1])
    plt.xlabel("Edge size")
    plt.ylabel("Probability of Concordance")
    plt.title(fr"$\sigma$ = {sigma}")
    plt.show()
    
    # frac_conc_lim is the limit of P[e concordant | size(e) = n] as n-->infty
    if sigma > Hbc.bound:
        frac_conc_lim = 0
    elif sigma < Hbc.bound:
        frac_conc_lim = 1
    else:
        frac_conc_lim = .5
        
    ratio = []
    nonzero_indices = []
    for n in range(2, N):
        if frac_conc[n] != 0:
            ratio.append(abs(frac_conc[n+1]-frac_conc_lim)/abs(frac_conc[n]- frac_conc_lim))
            nonzero_indices.append(n)
    plt.plot(nonzero_indices, ratio)   # if ratio is constant, then P[e concordant | size(e) = n] converges exponentially fast as n-->infty
    plt.title(fr"$\sigma$ = {sigma}, N = {N}")
    plt.xlabel("Edge size")
    plt.ylabel("Ratio of Concordance Probabilities")
    plt.show()

def plot_pn_error(sigma, N = 1000, num_trials = 2000, print_progress = True):
    # p_n is an approximation to the probability that the opinion of a node in 
    # a concordant size n hyperedge is farther than the confidence bound away from the 
    # hyperedge's mean opinion. Assumes opinions are normally distributed with
    # mean 0, standard deviation sigma. N is max edge size to consider. num_trials is
    # number of trials per edge size.
    p = norm.cdf(-1, scale = sigma) + 1 - norm.cdf(1, scale = sigma)    # probability that a node's opinion is farther than c away from mean of distribution
    print(f"p = {p}")
    count = np.zeros(N+1)
    prob = np.zeros(N+1)
    for n in range(2, N+1):
        print(n)
        for i in range(num_trials):
            # Generate a concordant size n hyperedge
            disc = Hbc.bound
            while disc >= Hbc.bound:
                opinions = np.random.normal(0, sigma, n)
                disc = 0
                mean_e = sum(opinions)/n
                for j in range(n):
                    disc += (opinions[j] - mean_e)**2
                disc /= (n-1)
            
            # Count the number of nodes in the hyperedge that are farther than
            # the confidence bound away from the hyperedge's mean opinion
            for j in range(n):
                if abs(opinions[j] - mean_e) > Hbc.bound:
                    count[n] += 1
        prob[n] = count[n]/(num_trials*n)
    error = [abs(p - prob[i]) for i in range(N+1)]
    
    # Plot |p - p_n| as a function of edge size n, where p_n and p are defined as in paper.
    plt.plot(range(2, N+1), error[2: N+1], 'o')
    plt.title(fr"{num_trials} trials per edge size, $\sigma$ = {sigma}")
    plt.xlabel(r"Edge size $n$")
    plt.ylabel(r"$|p_n - p|$")
    plt.show()
    
    # Plot n*|p- p_n|
    err = [i*error[i] for i in range(N+1)]
    plt.plot(range(2, N+1), err[2 : N+1], 'o')
    plt.title(fr"{num_trials} trials per edge size, $\sigma$ = {sigma}")
    plt.xlabel(r"Edge size $n$")
    plt.ylabel(r"$n*|p_n - p|$")
    plt.show()

# Figures
complete_sim_plot(Distribution('normal', sigma = 1.2), 500) # Figure 1
plot_avg_conc_edge_size(500, 1.2, 10000)    # Figure 2
hsbm_sim(Distribution('uniform', a = -2.2, b = -1.8), Distribution('uniform', a = 1.8, b = 2.2), 500)   # Figure 3
Gnm_sim_plot(Distribution('uniform', a = -2, b = 2))    # Figure 4a
Gnm_sim_plot(Distribution('normal', sigma = 1.2))   # Figure 4b
enron_sim_plot(Distribution('uniform')) # Figure 5a
enron_sim_plot(Distribution('normal'))  # Figure 5b
jump_sim()  # Figure 7

# Other numerical simulations
complete_sim(Distribution('normal', sigma = 1.2), 200)  # Complete graph, normally distributed initial opinions with variance > c
Gnm_sim(Distribution('uniform', a = -2, b = 2)) # Section 3.3 simulations
Gnm_sim(Distribution('normal', sigma = 1.2))    # Section 3.3 simulations
enron_sim(Distribution('uniform'))  # Section 3.4 simulations
enron_sim(Distribution('normal'))   # Section 3.4 simulations
plot_prob_conc(500, 50000, Hbc.bound)  # Remark 4.6
plot_pn_error(Hbc.bound - .1)   # Remark 5.4
plot_pn_error(Hbc.bound)    # Remark 5.4
plot_pn_error(Hbc.bound + .1)   # Remark 5.4