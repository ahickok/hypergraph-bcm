# -*- coding: utf-8 -*-
"""
Hypergraph Bounded Confidence Model
"""
import numpy as np
import random
import math
from scipy.special import comb
import matplotlib.pyplot as plt

class Distribution:
    def __init__(self, dist_type, mean = 0, sigma = 1, a = 0, b = 1):
        # dist_type is 'uniform' or 'normal'. If 'uniform', then a and b are 
        # the lower and upper bounds, resp. If 'normal', then mean and sigma are
        # the mean and standard deviation, resp.
        self.dist_type = dist_type
        if dist_type == 'uniform':
            self.a = a
            self.b = b
        else:
            self.mean = mean
            self.sigma = sigma
    
    def generate(self, N = 1):
        if self.dist_type == 'uniform':
            return (self.b-self.a)*np.random.random(N) + self.a
        else:
            return np.random.normal(self.mean, self.sigma, N)
        
class Hbc:
    bound = 1
    alpha = 1
    
    def __init__(self, N, opinions = None, edges = None, on_the_fly = False, p = 1, q = None, hsbm_bounded = False):
        '''
        Parameters
        ----------
        N : Number of nodes.
        edges : List of lists
            edges[n] = list of edges with n nodes that are in hypergraph. If
            on_the_fly = True, edges can be list of empty edges or seeded with
            a few initial edges.
        opinions : List/array of opinions, length N.
        on_the_fly : Boolean, optional
            False if set of edges is already decided before simulation. 
            True if we render the hypergraph on the fly as we're choosing edges 
            at each time step. Should be set to False if the graph is sparse enough.
        p : Integer where p = probability of any given hyperedge existing,
            or p is an array of length N-1 where p[n] = probability of a hyperedge 
            of dimension (n+2) existing.
        '''
        self.N = N
        
        if opinions is None:
            self.opinions = np.random.rand(N)
        else:
            self.opinions = [float(op) for op in opinions] 
            
        self.record = self.opinions
        
        if edges is None:
            self.edges = [[] for i in range(N-1)]
        else:
            self.edges = edges
            
        self.on_the_fly = on_the_fly
        self.hsbm_bounded = hsbm_bounded
        if on_the_fly:
            # This is the only situation where we need to store self.hsbm
            if q is None:
                # In this case we're doing G(N, p)
                self.hsbm = False
                if isinstance(p, float) or isinstance(p, int):
                    self.p = np.full(N-1, p)
                else:
                    self.p = p
                    for i in range(len(p), N-1):
                        self.p.append(0)
            else:
                # in this case, we're doing HSBM. Assume p and q are floats
                if hsbm_bounded:
                    # Don't need to store self.hsbm
                    # In this case assume p = 1 and q = 1.
                    self.p = 1
                    self.q = 1
                    
                    # Fraction of hyperedges that are intra-community edges
                    self.intra_fraction = (math.comb(N, 2) - 2*math.comb(int(N/2), 2))/(2*(2**(N/2) - N/2 - 1))
                else:
                    self.hsbm = True
                    self.p = p
                    self.q = q

            # Edges that we've generated at a previous time step and decided are not in the hypergraph.
            self.not_edges = [[] for i in range(N-1)]
    
    def add_edge(self, edge):
        '''
        Adds an edge to the list of edges, keeps the list of edges sorted. 
        '''
        n = len(edge)
        edge.sort()
        dim_n_edges = self.edges[n-2]
        if len(dim_n_edges) == 0:
            self.edges[n-2].append(edge)
        else:
            i, has_edge = Hbc.get_index(edge, dim_n_edges, 0, len(dim_n_edges)-1)
            if not has_edge:
                self.edges[n-2].insert(i, edge)
                
    def add_not_edge(self, edge):
        '''
        Adds an edge to the list of not edges, keeps the list of not_edges sorted. 
        Only allows you to add an edge if on_the_fly = True.
        '''
        if self.on_the_fly:
            n = len(edge)
            dim_n_not_edges = self.not_edges[n-2]
            if len(dim_n_not_edges) == 0:
                self.not_edges[n-2].append(edge)
            else:
                i, has_edge = Hbc.get_index(edge, dim_n_not_edges, 0, len(dim_n_not_edges)-1)
                if not has_edge:
                    self.not_edges[n-2].insert(i, edge)

    def approx_complete_conc_edge_size(N, sigma, num_trials, print_progress = True):
        # Returns list avg[m] = approximate average concordant edge size on the complete hypergraph m nodes (m <= N)
        # and list frac_conc[n] = approx fraction of edges size n that are concordant
        frac_conc = np.zeros(N)
        for n in range(2, N):
            if n%20 == 0: print(n)
            for i in range(num_trials):
                samp = np.random.normal(0, sigma, n)
                samp_avg = sum([samp[j] for j in range(n)])/n
                div = math.pow(n-1, Hbc.alpha)
                disc = sum([(samp[j] - samp_avg)**2 for j in range(n)])/div
                if disc < Hbc.bound:
                    frac_conc[n] += 1
            frac_conc[n] /= num_trials
        if print_progress: print("done with frac_conc")
        num_conc_edges = np.zeros(N)
        for m in range(2, N):
            # total num conc edges in complete hypergraph on m nodes
            num_conc_edges[m] = sum([frac_conc[n]*math.comb(m, n) for n in range(2, m)])
        if print_progress: print("done with num_conc")
        avg = np.zeros(N)
        for m in range(2, N):
            if not num_conc_edges[m] == 0:
                avg[m] = sum([n*frac_conc[n]*math.comb(m, n) for n in range(2, m)])/num_conc_edges[m]
        if print_progress: print("done with avg")
        return avg, frac_conc, num_conc_edges
    
    def average_opinion(self, edge):
        avg_opinion = sum([self.opinions[i] for i in edge])/len(edge)
        return avg_opinion
                        
    def benson_graph(nVerts_filename, sim_vertices_filename, dist):
        # dist is Distribution object
        nVerts_file = open(nVerts_filename, 'r')
        nVerts = nVerts_file.read().splitlines()
        nVerts = np.array([int(nv) for nv in nVerts])   # lists the number of vertices in each simplex
        simplices_file = open(sim_vertices_filename, 'r')
        sim_vertices = simplices_file.read().splitlines()
        sim_vertices = [int(sv)-1 for sv in sim_vertices] # first nVerts[0] vertices belong to 0th simplex, etc. Shift indices down so first node is node 0.
        
        n = max(sim_vertices) - min(sim_vertices) + 1
        deg = np.zeros(n)
        i = 0
        for nv in nVerts:
            if nv > 1:
                for j in range(i, i + nv):
                    deg[sim_vertices[j]] += 1
            i += nv
        idx = np.zeros(n)
        i = 0
        for j in range(n):
            if deg[j] > 0:
                idx[j] = i
                i += 1
        nNodes = len(deg[deg > 0])
        opinions = dist.generate(nNodes)
        graph = Hbc(nNodes, opinions)
        i = 0
        for nv in nVerts:
            if nv > 1:
                graph.add_edge([int(idx[sim_vertices[j]]) for j in range(i, i + nv)])
            i += nv
        return graph

    def compute_discordance(self, edge):
        try:
            if len(edge) < 2:
                return 0
            else:
                avg_opinion = self.average_opinion(edge)
                opinions = self.opinions
                discordance = 0
                div = math.pow(len(edge)-1, Hbc.alpha)
                for i in edge:
                    discordance += (opinions[i]-avg_opinion)**2
                discordance /= div
            return discordance
        except TypeError:
            print("type error")
            print(edge)
            edge = self.select_random_edge()
            return self.compute_discordance(edge)
    
    def consensus(self, tol = 1e-8):
        return np.var(self.opinions) < tol
    
    def converged(self, tol=1e-8):
        # Should only be called if on_the_fly = False so that we have a
        # pre-determined edge list.
        if self.on_the_fly:
            print("converged method should not be called if on_the_fly = True")
            return self.consensus()
        else:
            edges = [e for dim_n_list in self.edges for e in dim_n_list]
            for e in edges:
                if tol < self.compute_discordance(e) < Hbc.bound:
                    return False
        return True

    def degree(self):
        deg = np.zeros(self.N)
        for dim_n_list in self.edges:
            for e in dim_n_list:
                for i in e:
                    deg[i] += 1
        return deg
        
    def get_index(edge, edges, i, j):
        '''
        If edge is in edges, returns [index, True]. If edge is not in edges, returns 
        [index where it should go, False]. We are searching in edges[i], ..., edges[j].
        '''
        nNodes = len(edge)
        midIdx = math.floor((j-i)/2) + i # Rounds down. Also note if i = j, then midIdx = i.
        midEdge = edges[midIdx]
        
        k = 0 
        while k < nNodes and midEdge[k] == edge[k]:
            k += 1
        if k == nNodes: return midIdx, True
        elif i == j:
            if midEdge[k] < edge[k]: return i+1, False
            else: return i, False
        else:
            if midEdge[k] < edge[k]: 
                return Hbc.get_index(edge, edges, midIdx + 1, j)
            else: 
                if midIdx == i: return i, False
                else: return Hbc.get_index(edge, edges, i, midIdx -1)
                        
    def Gnm(N, m, opinions = None):
        # Create graph where there are m[i-2] random edges of size i.
        # Edges will not be sorted, but this doesn't matter since on_the_fly = False
        if isinstance(m, float) or isinstance(m, int):
                m = np.full(N-1, m)
        nodes = list(range(N))
        edges = [[0 for i in range(m[n])] for n in range(N-1)]
        for n in range(N-1):
            for i in range(m[n]):
                e = random.sample(nodes, n+2)
                edges[n][i] = tuple(e)
            edges[n] = list(set(edges[n]))
        graph = Hbc(N, opinions, edges)
        return graph

    def has_any_edges_avail(self):
        N = self.N
        if not self.on_the_fly:
            for n in range(N-1):
                if len(self.edges[n]) > 0: return True
            return False
        else:
            for n in range(N-1):
                if not len(self.not_edges[n]) == comb(N, n+2): return True
            return False
    
    def has_dim_n_edge(edge, edges):
        '''
        Edges: a list of hyperedges of the same dimension as edge.
        Edge: the edge we're binary searching for.
        '''
        nEdges = len(edges)
        if nEdges == 0:
            return False
        nNodes = len(edge)
        midIdx = math.floor((nEdges-1)/2)
        midEdge = edges[midIdx]
        
        i = 0 
        while i < nNodes and midEdge[i] == edge[i]:
            i += 1
            
        if i == nNodes:
            return True
        if nEdges <= 1:
            return False
        if midEdge[i] < edge[i]:
            return Hbc.has_dim_n_edge(edge, edges[midIdx + 1: nEdges])
        else:
            return Hbc.has_dim_n_edge(edge, edges[0 : midIdx])
    
    def has_edge(self, edge):
        '''
        edge: array of nodes in the edge
        Returns true if edge is in self.edges
        '''
        n = len(edge)
        if n < 2:
            return False
        if not self.on_the_fly:
            if n - 2 >= len(self.edges):
                return False
            else:
                dim_n_edges = self.edges[n-2]
                return Hbc.has_dim_n_edge(edge, dim_n_edges)

    def is_consensus_possible(self, tol = .1):
        # If a single consensus is reached, it will be at the initial mean.
        # Assumes mean opinion = 0.
        # Loop through all nodes n. If |opinions(n)| > tol, loop through all edges
        # incident to it and see if any are concordant, and if so what the mean is
        edges = [e for dim_n_list in self.edges for e in dim_n_list]
        num_edges_incident = np.zeros(self.N) # num_edges_incident[n] = number of edges incident to node n. Only keep track of for the nodes n that aren't at consensus
        num_edges_conc = np.zeros(self.N)   # num_edges_conc[n] = number of edges incident to node n that are concordant. Only keep track of for the nodes n that aren't at consensus
        not_converged = []  # list of nodes that aren't at consensus
        for n in range(self.N):
            if abs(self.opinions[n]) > tol:
                not_converged.append(n)
                for e in edges:
                    if n in e:
                        num_edges_incident[n] += 1
                        if self.compute_discordance(e) < Hbc.bound:
                            num_edges_conc[n] += 1
        for n in not_converged:
            print("Node ", n)
            if not self.on_the_fly: 
                print(num_edges_incident[n], " edges incident to node ", n)
            print("Number of concordant incident edges: ", num_edges_conc[n])
            print("Proportion concordant: ", num_edges_conc[n]/num_edges_incident[n])
            print("\n")
            
    def not_has_edge(self, edge):
        '''
        edge: array of nodes in the edge
        If not on_the_fly, returns true if edge isn't in self.edges
        If on_the_fly, returns true if edge is in self.not_edges
        '''
        n = len(edge)
        if n < 2:
            return True
        if not self.on_the_fly:
            return not self.has_edge(self, edge)
        else:
            if n - 2 >= len(self.not_edges):
                return False
            else:
                dim_n_not_edges = self.not_edges[n-2]
                return Hbc.has_dim_n_edge(edge, dim_n_not_edges)
                        
    def ordinary_complete_graph(n, opinions = None):
        # Generates complete ORDINARY graph (no hyperedges of size > 2)
        dim_2_edges = [[i, j] for i in range(n) for j in range(i+1, n)]
        graph = Hbc(n, opinions)
        graph.edges[0] = dim_2_edges
        Hbc.bound = 1/math.sqrt(2)
        return graph

    def plot_conc_edge_means(self, t, title = ""):
        if not self.on_the_fly:
            edges = [e for dim_n_list in self.edges for e in dim_n_list]
            conc_means = [sum([self.opinions[i] for i in e])/len(e) for e in edges if self.compute_discordance(e) < Hbc.bound]
        else:
            print("this is only for complete hypergraphs")
            N = self.N
            num_trials = 100
            conc_means = np.zeros(num_trials)
            for i in range(num_trials):
                e = [i for  i in range(N) if random.random() < .5]
                disc = self.compute_discordance(e)
                while disc > Hbc.bound:
                    e = [i for  i in range(N) if random.random() < .5]
                    disc = self.compute_discordance(e)
                print(i)
                conc_means[i] = self.average_opinion(e)
        plt.hist(conc_means, bins = 100)
        if not len(title) == 0:
            plt.suptitle(title)
        plt.title(f"Distribution of Concordant Edge Means, t = {t}")
        plt.show()
        return conc_means
        
    def plot_edge_dist(self, title = None):
        m = [len(self.edges[i]) for i in range(self.N-1)]
        max_nonzero_idx = 0
        for i in range(len(self.edges)):
            if not m[i] == 0:
                max_nonzero_idx = i
        print(max_nonzero_idx)
        edge_sizes = [i+2 for i in range(max_nonzero_idx) for j in range(m[i])]
        plt.hist(edge_sizes, bins = self.N-1)
        if title is not None: 
            plt.suptitle(title)
        plt.title("Edge Size Distribution")
        plt.show()
        
    def plot_record(self, first_conc = False, sigma = None, title = None, filename = None):
        # If sigma = None, assume uniformly distributed initial opinions.
        mean_opinion = np.mean(self.opinions)
        plt.plot(self.record.T)
        if title is not None: plt.suptitle(title)
        if first_conc:
            if sigma is None:
                plt.title(f"First edge chosen is concordant, N = {self.N}")
            else:
                plt.title(f"First edge chosen is concordant, N = {self.N}, sigma = {sigma}")
        else:
            if sigma is None:
                plt.title(f"Uniformly Distributed Initial Opinions, N = {self.N}")
            else:
                plt.title(f"N = {self.N}, sigma = {sigma}")
        plt.xticks(fontsize = 18)
        plt.yticks(fontsize = 18)
        plt.xlabel(r"$t$", fontsize = 28)
        plt.ylabel(r"$x_i$", fontsize = 28)
        plt.hlines(mean_opinion, 0, len(self.record.T), colors='r', linestyles='dashed')
        if filename is not None: plt.savefig(filename)
       	#plt.clf()
        plt.show()
        
    def polarized(self, tol = 1e-8):
        # Assumes nodes 0,..., N/2 - 1 are in community 1 and nodes N/2, ..., N-1 are in community 2
        return (np.var(self.opinions[:int(self.N/2)]) < tol and np.var(self.opinions[int(self.N/2):]) < tol)

    def reset(self, opinions = None):
        if opinions is None:
            self.opinions = self.record[:, 0]
        else:
            self.opinions = opinions
        self.record = self.opinions
        
    def select_rand_conc_edge(self):
        # Pick an edge that is concordant
        e = self.select_random_edge()
        disc = self.compute_discordance(e)
        while disc >= Hbc.bound:
            e = self.select_random_edge()
            disc = self.compute_discordance(e)
        return e
               
    def select_random_edge(self):
        if not self.has_any_edges_avail():
            print("no edges available")
            return []
        else:
            e = []
            N = self.N
            if not self.on_the_fly:
                e = random.choice([e for dim_n_list in self.edges for e in dim_n_list])
                return e
            elif self.hsbm_bounded:
                # HSBM(p, q, M) where p = 1, q = 1, and M = 2.
                if random.random() < self.intra_fraction:
                    # Pick intra-community edge
                    i = random.randint(1, int(N/2) - 1) # node in community 1
                    j = random.randint(int(N/2), N-1)   # node in community 2
                    return [i, j]
                else:
                    # Pick inter-community edge
                    if random.random() < .5:
                        # Pick edge in community 1.
                        e = [i for i in range(int(N/2)) if random.random() < .5]
                    else:
                        # Pick edge in community 2.
                        e = [i for i in range(int(N/2), N) if random.random() < .5]
                    return e
            else:
                # Either G(N, p) or HSBM(p, q).
                e = [i for i in range(N) if random.random() < .5]
                if self.has_edge(e):
                    # generated e before and it's in the graph
                    return e
                elif self.not_has_edge(e):
                    # either generated e before and it's not in the graph, or len(e) < 2
                    self.select_random_edge()
                else:
                    # generated e for the first time
                    n = len(e)
                    if self.hsbm:
                        # Assume self.N is even
                        mid_node = int(self.N/2)    # nodes 0, ..., self.N/2 -1 are in community 1. nodes self.N/2, ..., self.N -1 are in comm 2.
                        
                        # Check if e is entirely contained in a community
                        for i in range(n-1):
                            #comm = ((e[i] < self.N) == (e[i+1] < self.N))
                            comm = ((e[i] < mid_node) == (e[i+1] < mid_node))
                            if comm == False: break
                        if (comm and random.random() < self.p) or (not comm and random.random() < self.q):
                            self.add_edge(e)
                            return e
                        else:
                            self.add_not_edge(e)
                            return self.select_random_edge()
                        
                    else:
                        if random.random() < self.p[n-2]:
                            # put e into the graph
                            self.add_edge(e)
                            return e
                        else:
                            # put e into the list of not_edges
                            self.add_not_edge(e)
                            return self.select_random_edge()

    def simulate(self, T, track_var = False, print_progress = True):
        N = self.N
        T = int(T)
        self.record = np.zeros((N,T+1))
        self.record[:,0] = self.opinions
        if track_var:
            var = np.zeros(T+1)
            var[0] = np.var(self.opinions)
        for t in range(T):
            if print_progress and (t < 100 or t%1000 == 0): print(t)
            edge = self.select_random_edge()
            if 0 < self.compute_discordance(edge) < Hbc.bound:
                self.update_group(edge)
            self.record[:,t+1] = self.opinions
            if track_var: var[t+1] = np.var(self.opinions)
        if track_var: 
            return var
    
    def simulate_until(self, condition, first_conc = False, tol = 1e-8, MaxT = int(1e5), print_progress = True,
                       track_var = False, track_edge_updates = False):
        # condition is boolean function
        N = self.N
        self.record = np.zeros((N, MaxT+1))
        self.record[:, 0] = self.opinions
        if track_var:
            var = np.zeros(MaxT + 1)
            var[0] = np.var(self.opinions)
        if track_edge_updates: 
            edge_update_size = np.zeros(MaxT + 1)
            edge_update_mean = np.zeros(MaxT + 1)
            
        t = 0
        while not condition(self, tol) and t < MaxT:
            if print_progress and t%1000 == 0: print(t)
            if first_conc and t == 0:
                edge = self.select_rand_conc_edge()
                if print_progress: print("found first concordant hyperedge")
            else:
                edge = self.select_random_edge()
            if 0 < self.compute_discordance(edge) < Hbc.bound:
                self.update_group(edge)
                if track_edge_updates:
                    edge_update_size[t] = len(edge)
                    edge_update_mean[t] = sum([self.opinions[i] for i in edge])/len(edge)
            self.record[:, t+1] = self.opinions
            if track_var: var[t+1] = np.var(self.opinions)
            t += 1
        self.record = self.record[:, :(t+1)]
        if track_var: var = var[0: t+2]
        if track_edge_updates:
            edge_update_size = edge_update_size[:t+1]
            edge_update_mean = edge_update_mean[:t+1]
        if track_var and track_edge_upates:
            return var, edge_update_size, edge_update_mean
        elif track_var:
            return var
        elif track_edge_updates:
            return edge_update_size, edge_update_mean
    
    def simulate_until_no_record(self, condition, tol=1e-8, MaxT=int(1e6), print_progress = True):
        no_updates = True
        t = 0
        while not condition(self, tol) and t < MaxT:
            if print_progress and t%100000 == 0: print(t)
            edge = self.select_random_edge()
            if 0 < self.compute_discordance(edge) < Hbc.bound:
                self.update_group(edge)
                if no_updates:
                    if print_progress: print("first update")
                    no_updates = False
            t += 1
        if print_progress and t < MaxT: print("converged")
        return t, no_updates
    
    def sparse(N, m, opinions):
        edges = [set() for n in range(N-1)]
        nodes = list(range(N))
        for n in range(N-1):
            while len(edges[n]) < min(m, math.comb(N, n+2)):
                e = random.sample(nodes, n+2)
                edges[n].add(tuple(e))
            edges[n] = list(edges[n])
        return Hbc(N, opinions, edges)

    def update_group(self, edge):
        avg_opinion = self.average_opinion(edge)
        for i in edge:
            self.opinions[i] = avg_opinion