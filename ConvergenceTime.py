'''
Numerical simulation for computing convergence time as a function of the variance in the initial opinion distribution.
(Figure 6 in paper)
Written by Yacoub Kureh.
'''
import numpy as np
import matplotlib
matplotlib.use('Agg')
import datetime
import matplotlib.pyplot as plt
import random
import multiprocessing
from multiprocessing import Pool
plt.rcParams.update({'font.size': 18})

def average_opinion(opinions,edge):
    return sum(opinions[i] for i in edge)/len(edge)
  

def compute_discordance(opinions,edge,alpha):
    avg_opinion = average_opinion(opinions,edge)
    discordance = sum((opinions[i]-avg_opinion)**2 for i in edge)
    div = (len(edge)-1)
    return discordance/div
    
def update_group(opinions,edge,multiplier):
    avg_opinion = average_opinion(opinions,edge)
    for i in edge:
        opinions[i] = avg_opinion
        

def select_random_edges(N): 
    return [i for i in range(N) if random.getrandbits(1)==1]

def simulate_until_converge_no_record(opinions,tol=1e-8,MaxT=int(1e4)):
    N = len(opinions)
    for t in range(MaxT):
        edges = select_random_edges(N)
        if 0 < compute_discordance(opinions,edges,None) < 1:
            update_group(opinions,edges,None)
        if compute_discordance(opinions,list(range(N)),None) < tol:
            return t
    return MaxT

N = 50000
d_sigma=50
trials = 20

s = np.linspace(1-0.1, 1+0.1, num=d_sigma)
means = np.zeros((d_sigma*trials,N))
stddevs = np.tile(np.array([np.repeat(s,trials)]).transpose(),(1,N))
opinions_list = np.random.normal(means,stddevs).tolist()


end_times =[]
with Pool(d_sigma) as p:
    end_times.append(p.map(simulate_until_converge_no_record,opinions_list))

end_times = np.array(end_times)
end_times = np.reshape(end_times,(d_sigma,trials)).transpose()

mean_times = np.mean(end_times,axis=0)
stderr_times = np.std(end_times,axis=0)

min_val = np.clip(mean_times-stderr_times, 0, None)

plt.plot(s, mean_times, 'k-')
plt.fill_between(s, min_val, mean_times+stderr_times)
gamma = (1-(4/(9*50000)))**(-3/2)
plt.vlines(gamma,-2000,12000,colors='r',linestyles='dashed')
plt.xlabel(r"$\sigma$")
plt.ylabel(r"$t^*$")
plt.savefig(f"Figures/DW_hypergraph_consensus_time_{N}_Normal_Opinons_DelSigma_{d_sigma}_trials_{trials}.pdf",bbox_inches='tight')